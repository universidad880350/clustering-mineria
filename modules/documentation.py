from typing import Callable


class ExternalLink:
    """
    Contains a module name and the url to the documentation.

    Attributes:
        module: Name of the module.
        url: link to the module documentation.
    """

    def __init__(self, module_name: str, url: str):
        self.module = module_name
        self.url = url

    def __str__(self):
        return "[`{}`]({})".format(self.module, self.url)


class ExternalLinks:
    """
    Contains the external links to the external used modules.
    """
    pandas_data_frame = ExternalLink("pandas.core.frame.DataFrame",
                                     "https://pandas.pydata.org/pandasdocs/stable/"
                                     "reference/api/pandas.DataFrame.html")
    pandas_series = ExternalLink("pandas.core.frame.DataFrame",
                                 "https://pandas.pydata.org/pandas-docs/stable"
                                 "/reference/api/pandas.Series.html")
    gensim_doc_to_vec = ExternalLink("gensim.models.doc2vec.Doc2Vec",
                                     "https://radimrehurek.com/gensim/models/doc2vec.html")


def docstring_parameter(*args, **kwargs) -> Callable:
    """
    Inserts into the docstring formatted external links.
    Args:
        *args: nameless link.
        **kwargs: link with parameter name.

    Returns:
        The decorator
    """

    def dec(obj):
        """
        Formats the docstring with the parameters.
        Args:
            obj: object to be modified the docstring.

        Returns:
            The Modified obj.
        """
        obj.__doc__ = obj.__doc__.format(*args, **kwargs)
        return obj

    return dec


def docstring_inherit(parent) -> Callable:
    """
    Inserts into the docstring parents Attributes.
    Args:
        parent: the objects parent class.

    Returns:
        The decorator
    """

    def inherit(obj):
        """
        Inserts into the inherited class the parent attributes'.
        Args:
            obj: The child class obj.

        Returns:
            The obj with the modified docstring.
        """
        spaces = "    "
        if not str(obj.__doc__).__contains__("Attributes:"):
            obj.__doc__ += "\n" + spaces + "Attributes:\n"
        obj.__doc__ = str(obj.__doc__).rstrip() + "\n"
        for attribute in parent.__doc__.split("Attributes:\n")[-1].lstrip().split("\n"):
            obj.__doc__ += spaces * 2 + str(attribute).lstrip().rstrip() + "\n"
        return obj

    return inherit
