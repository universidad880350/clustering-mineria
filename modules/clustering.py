from typing import Dict, List, Tuple, Union, Optional, ValuesView, KeysView

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram

from modules import get_config
from modules.containers import MyDataFrame, Node, ClusterNode, InstanceNode
from modules.documentation import docstring_inherit, docstring_parameter, \
    ExternalLinks
from modules.evaluation import InfoDataFrame
from modules.models import KMeans, DocToVec, Standardization, PCA
from modules.settings import Settings
from modules.useful import get_distances_centroid_to_centroids, add_prefix, \
    rm_prefix, load_nlp, clean_instance, get_matrix_distances


@docstring_inherit(MyDataFrame)
@docstring_parameter(ExternalLinks.pandas_data_frame)
class UnprocessedDataFrame(MyDataFrame):
    """
    Pandas container for easier data preprocessing. Contains a raw {}
    and retrieves the useful cols. The contained data format is:

    | Original Title | Overview | Genres | Original_Language |
    | :---:  | :--- | :---:  | :---:  |
    | Ariel  Taisto  | Kasurinen is a Finnish coal miner whose...  | Drama & Crime   | fi  |
    | Four Rooms  | It's Ted the Bellhop's first night on the job....  | Crime & Comedy  | en  |
"""

    def __init__(self, my_data_frame: MyDataFrame):
        super().__init__(my_data_frame.dataFrame)
        self.update_data_frame(
            self.get_sub_data_frame(
                [Settings.film_col, Settings.description_col, Settings.types, Settings.language]))
        self.set_dtypes({Settings.description_col: str, Settings.types: str})

    @staticmethod
    def get_data_frame(input_path: str, sep: str = ",", ) -> 'UnprocessedDataFrame':
        """
        Retrieves the dataframe from a given csv path and generates a instance
        Args:
            input_path: path to the csv
            sep: the separator of the csv cols

        Returns:
            The container of the retrieved dataFrame
        """
        return UnprocessedDataFrame(MyDataFrame.get_data_frame(input_path, sep))

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def get_subset(self, percentage: float, drop: bool = False) -> 'UnprocessedDataFrame':
        """
        It returns a new container with a subset of the {}, if drop is set to True it
        removes the generated subset in the instance.
        Args:
            percentage: It corresponds to the relative size of the subset
            drop: If the instance should pop or get the subset

        Returns:
            The container of the subset
        """
        return UnprocessedDataFrame(super().get_subset(percentage, drop))

    def get_genres(self) -> Dict[str, int]:
        """
        Gets all the genres found and transforms the genre col into multiple columns containing 1
        or 0 depending if the instance contains that col genre.
        Returns:
            A dictionary with the genre and the count array
        """
        all_genres = []
        [[all_genres.append(genre) for genre in genres.split("|")
          if genre not in all_genres] for genres in self.get_sub_data_frame(Settings.types)]

        genre_dict = {genre: np.zeros(len(self), dtype=np.int16) for genre in all_genres}
        for i, genres in enumerate(self.get_sub_data_frame(Settings.types)):
            for genre in genres.split("|"):
                genre_dict[genre][i] = 1
        return genre_dict

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def preprocess(self) -> 'UncleanedDataFrame':
        """
        Preprocesses the dataFrame and converts it into `UncleanedDataFrame` format.

        - Removes non english films, and missing instances
        - Transforms the genre col into an easier structure to handle
        - Drops language and genre col

        Returns:
            The generated {} in `UncleanedDataFrame` format
        """
        self.update_data_frame(self.get_sub_data_frame(
            ~self.get_sub_data_frame(
                Settings.description_col).str.lower().str.contains("no overview", na=False)))
        self.update_data_frame(
            self.get_sub_data_frame(self.get_sub_data_frame(Settings.language) == "en"))
        self.drop_na()
        genre_dict = self.get_genres()
        my_data_frame = self.get_copy()
        [my_data_frame.update_sub_data_frame(Settings.prefix_t + genre, genre_dict[genre]) for genre
         in genre_dict.keys()]
        my_data_frame.drop_fields(Settings.types, axis=1)
        my_data_frame.drop_fields(Settings.language, axis=1)
        return UncleanedDataFrame(my_data_frame)


@docstring_inherit(MyDataFrame)
@docstring_parameter(ExternalLinks.pandas_data_frame)
class UncleanedDataFrame(MyDataFrame):
    """
    Container of {} for better cleaning of the dataFrame.
    Format:

    | Original Title | Overview | genre_Crime | ... | genre_War |
    | :---:  | :--- | :---:  | :---:  | :---:  |
    | Last Summer  | An island with secrets.  | 0 | ...  | 0 |
    | Penarek Becha  | The story tells us about a trishaw puller....  | 0 | ...  | 0 |
    """

    def __init__(self, my_data_frame: MyDataFrame):
        super().__init__(my_data_frame.dataFrame)
        self.nlp = load_nlp(get_config().spacy_model)

    @staticmethod
    def get_data_frame(input_path: str, sep: str = ",", ) -> 'UncleanedDataFrame':
        """
        Retrieves the dataframe from a given csv path and generates a instance
        Args:
            input_path: path to the csv
            sep: the separator of the csv cols

        Returns:
            The container of the retrieved dataFrame
        """
        return UncleanedDataFrame(MyDataFrame.get_data_frame(input_path, sep))

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def get_subset(self, percentage: float, drop: bool = False) -> 'UncleanedDataFrame':
        """
        It returns a new container with a subset of the {}, if drop is set to True it
        removes the generated subset in the instance.
        Args:
            percentage: It corresponds to the relative size of the subset.
            drop: If the instance should pop or get the subset

        Returns:
            The container of the subset
        """
        return UncleanedDataFrame(super().get_subset(percentage, drop))

    # noinspection PyTypeChecker
    def clean(self):
        """
        Cleans the current data's overview replacing the identified entities and the lemmas of
        the words.

        Returns:
            The generated `InstancesDataFrame` container
        """
        my_data_frame = self.get_copy()
        my_data_frame.update_sub_data_frame(Settings.modified_col,
                                            my_data_frame.get_sub_data_frame(
                                                Settings.description_col).apply(
                                                lambda x: clean_instance(self.nlp, x)))
        return InstancesDataFrame(my_data_frame)


@docstring_inherit(MyDataFrame)
@docstring_parameter(ExternalLinks.pandas_data_frame)
class InstancesDataFrame(MyDataFrame):
    """
    A Container for better vectorization of the instances on {} format.
    Format:

    | id | original_title | overview | genre_Crime | ... | genre_TV Movie | modified |
    | :---: | :---: | :--- | :---: | :---: | :---: | :---: |
    | 448318 | Last Summer | An island with secrets. | 0 | ... | 0 | ['An', 'island', ...] |
    | 70219 | Penarek Becha | The story tells us abou... | 0 | ... | 0 | ['The', 'story', ...] |


    """

    def __init__(self, my_data_frame: MyDataFrame):
        super().__init__(my_data_frame.dataFrame)

    @staticmethod
    def get_data_frame(input_path: str, sep: str = ",", ) -> 'InstancesDataFrame':
        """
        Retrieves the dataframe from a given csv path and generates a instance
        Args:
            input_path: path to the csv
            sep: the separator of the csv cols

        Returns:
            The container of the retrieved dataFrame
        """
        return InstancesDataFrame(MyDataFrame.get_data_frame(input_path, sep))

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def get_subset(self, percentage: float, drop: bool = False) -> 'InstancesDataFrame':
        """
        It returns a new container with a subset of the {}, if drop is set to True it
        removes the generated subset in the instance.

        Args:
            percentage: It corresponds to the relative size of the subset
            drop: If the instance should pop or get the subset

        Returns:
            The container of the subset
        """
        return InstancesDataFrame(super().get_subset(percentage, drop))

    def get_cluster_list(self) -> List[str]:
        """
        Gets the list of the instances' names with a cluster prefix
        Returns:
            The generated list
        """
        return add_prefix(self.get_row_names(), Settings.prefix_c)

    def create_vec_data_frame(self) -> Tuple['InstancesVecDataFrame',
                                             DocToVec, Standardization]:
        """
        Trains a doc2vec model and returns the standardized generated representations of the
        instances, and the used models to achieve that.
        Returns:
            the standardized generated representations of the instances, and the used models
            to achieve that.
        """
        doc_to_vec = DocToVec.train_model(
            DocToVec.get_docs(self.get_sub_data_frame(Settings.modified_col)))
        # noinspection PyTypeChecker
        docs_vec = MyDataFrame(pd.DataFrame(doc_to_vec.get_vectors(), index=self.get_row_names(),
                                            columns=add_prefix(
                                                range(0, get_config().vector_size),
                                                Settings.prefix_v)))
        docs_vec.update_index_name(Settings.index_name)
        df_vec = self.merge(docs_vec, inplace=False)
        instances_vec_data_frame = InstancesVecDataFrame(
            df_vec.drop_fields(Settings.modified_col, axis=1, inplace=False))
        centroid = instances_vec_data_frame.get_centroid()
        standardization = Standardization.build_model(centroid)
        instances_vec_data_frame.update_sub_data_frame(instances_vec_data_frame.vector_cols,
                                                       standardization.standardize(centroid))
        return instances_vec_data_frame, doc_to_vec, standardization


# TODO: change dict with Dict
@docstring_parameter(ExternalLinks.pandas_data_frame)
@docstring_inherit(InstancesDataFrame)
class InstancesVecDataFrame(InstancesDataFrame):
    """
    Container of vec instances {} for easier transformation to distance matrix format
    Format

    | id | original_title | overview | genre_Crime | ... | vec_498 | vec_499 |
    | :---: | :---: | :--- | :---: | :---: | :---: | :---: |
    | 292468 | Urs Al-Zayn | Everyone in his village | 0 | ... | -1.238589 | -0.002429 |
    | 93767 | Seven Women from Hell | Seven women from... | 0 | ... | -0.090666 | 0.608749 |

    Attributes:
        vector_cols (list): The cluster representation col names.
        genre_cols (list): The cluster genres count col names.
    """

    def __init__(self, my_data_frame: MyDataFrame):
        super().__init__(my_data_frame)
        self.vector_cols = my_data_frame.filter(prefix=Settings.prefix_v, inplace=False,
                                                axis=1).get_col_names()
        self.genre_cols = my_data_frame.filter(prefix=Settings.prefix_t, inplace=False,
                                               axis=1).get_col_names()

    @staticmethod
    def get_data_frame(input_path: str,
                       sep: str = ",", ) -> 'InstancesVecDataFrame':
        """
        Retrieves the dataframe from a given csv path and generates a instance
        Args:
            input_path: path to the csv
            sep: the separator of the csv cols

        Returns:
            The container of the retrieved dataFrame
        """
        return InstancesVecDataFrame(MyDataFrame.get_data_frame(input_path, sep))

    def get_genre(self, row: Union[int, List[int]]) -> str:
        """
        Given the cluster id returns the most frequent genre.
        Args:
            row: The cluster id.

        Returns:
            The most frequent genre
        """
        return MyDataFrame(self.get_cell(row, self.genre_cols)).get_arg_max(axis=1).values[0]

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def get_subset(self, percentage: float, drop: bool = False) -> 'InstancesVecDataFrame':
        """
        It returns a new container with a subset of the {}, if drop is set to True it
        removes the generated subset in the instance.

        Args:
            percentage: It corresponds to the relative size of the subset
            drop: If the instance should pop or get the subset

        Returns:
            The container of the subset
        """
        return InstancesVecDataFrame(super().get_subset(percentage, drop))

    @docstring_parameter(ExternalLinks.pandas_data_frame, ExternalLinks.pandas_series)
    def get_centroid(self,
                     cls: Union[int, List[int]] = None) -> Optional[Union[pd.DataFrame, pd.Series]]:
        """
        Gets the vec representation of the requested clusters

        Args:
            cls: A subset of index

        Returns:
            The requested dataFrame
        """
        if cls is None:
            return self.dataFrame[self.vector_cols]
        else:
            return self.get_cell(cls, self.vector_cols)

    def get_distance_matrix(self) -> MyDataFrame:
        """
        Generates the distance matrix between the instances

        Returns:
            The generated distance matrix
        """
        # noinspection PyTypeChecker
        return MyDataFrame(
            pd.DataFrame(
                get_matrix_distances(self.get_centroid().values, get_config().minkowski_degree),
                columns=self.get_cluster_list(), index=self.get_row_names()).replace(
                {np.float64(0): np.nan}))

    def pca(self, dims: int = 2, output_folder: str = None) -> MyDataFrame:
        """
        Returns the spatial reduced representation of the current dataset

        Args:
            dims: The number of dimensions of the new representation
            output_folder: It will store the accumulative eigenvalues plot

        Returns:
            The generated representation
        """
        return PCA.pca_and_reduce_dims(self, self.get_centroid(), self.vector_cols, dims=dims,
                                       output_folder=output_folder)

    def plot(self, output_folder: str = None) -> None:
        """
        It will show or store the reduced representations of the instances in space

        Args:
            output_folder: if not none it will store the generated plot
        """
        df_reduced = self.pca(output_folder=output_folder)
        df_reduced.update_sub_data_frame(Settings.types,
                                         df_reduced.filter(Settings.prefix_t,
                                                           axis=1).get_arg_max(axis=1).values)
        PCA.plot(df_reduced, Settings.types, output_folder)

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def create_hierarchical_clustering(self) -> 'HierarchicalClustering':
        """
        Generates a container with a {} that handles the distances between clusters and a `Tree`
        for clustering the instances.

        Returns:
            The generated `HierarchicalClustering` container.
        """
        clusters = {
            str(index): InstanceNode(index, centroid.values, instance, genre.idxmax(), text,
                                     film_name)
            for (index, ((instance, centroid), (cls, genre), text, film_name))
            in enumerate(
                zip(self.get_sub_data_frame(self.vector_cols).iterrows(),
                    self.get_sub_data_frame(self.genre_cols).iterrows(),
                    self.get_sub_data_frame(Settings.description_col),
                    self.get_sub_data_frame(Settings.film_col)))}
        df_instances = InstancesVecDataFrame(self.get_copy())
        df_instances.drop_fields([Settings.description_col, Settings.film_col], axis=1,
                                 inplace=True)
        df_clusters = df_instances.merge(df_instances.get_distance_matrix(), inplace=False)
        df_clusters.update_sub_data_frame(Settings.n_instances,
                                          np.ones(shape=len(df_clusters), dtype=np.int16))
        return HierarchicalClustering(ClusteringIteration(df_clusters), clusters)


@docstring_inherit(InstancesDataFrame)
class ClusteringIteration(InstancesVecDataFrame):
    """
    The container of the state of the clusters with the distances for easier clustering
    Format:

    | id | genre_Comedy | genre_Action | ... | cluster_27565 | cluster_189813 | num_instances |
    | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
    | 61383 | 0 | 0 | ... | 18.890615 | 10.598184 | 1 |
    | 448318 | 0 | 0 | ... | 20.965397 | 24.902719 | 1 |

    Attributes:
        cluster_cols (list): The corresponding cols names of the distance matrix
        evaluator_metric (float): The unsupervised evaluation of the current state
        iteration (int): The clustering iteration.
        initial_num_clusters (int): The num instances of the train.
        min_distance (float): The min distance between the most near clusters
        purity (float): The supervised evaluation of the current state
    """

    def __init__(self, my_data_frame: MyDataFrame):
        super().__init__(my_data_frame)
        self.cluster_cols = add_prefix(my_data_frame.get_row_names(), Settings.prefix_c)
        self.evaluator_metric = 0.0
        self.iteration = 0
        self.initial_num_clusters = len(self)
        self.min_distance = 0.0
        self.purity = 100.0

    @staticmethod
    def get_data_frame(input_path: str, sep: str = ",", ) -> 'ClusteringIteration':
        """
        Retrieves the dataframe from a given csv path and generates a instance
        Args:
            input_path: path to the csv
            sep: the separator of the csv cols

        Returns:
            The container of the retrieved dataFrame
        """
        return ClusteringIteration(MyDataFrame.get_data_frame(input_path, sep))

    def get_new_cluster_index(self) -> int:
        """
        Gets the new cluster index

        Returns:
            The new cluster index
        """
        return self.iteration + self.initial_num_clusters

    def get_num_instances_in_cluster(self, cls_id: int) -> int:
        """
        Returns the num instances in the requested cluster

        Args:
            cls_id: cluster identification

        Returns:
            The num instances cell
        """
        return self.get_cell(cls_id, Settings.n_instances)

    def increase_iteration(self) -> None:
        """
        Increases the iteration
        """
        self.iteration += 1

    def update_centroid(self, cls_id, centroid: np.ndarray) -> None:
        """
        Given a centroid updates the requested cls_id representation
        Args:
            cls_id: it corresponds to the cluster identification
            centroid: the new cluster representation

        """
        self.update_cell(cls_id, self.vector_cols, centroid)

    def update_genres(self, cls_1: int, cls_2: int) -> None:
        """
        Integrates cl2 genres into the cls1 genres.

        Args:
            cls_1: it corresponds to the first cluster's id
            cls_2: it corresponds to the seconds cluster's id

        Returns:

        """
        self.update_cell(cls_1, self.genre_cols,
                         self.get_cell(cls_1, self.genre_cols).values
                         + self.get_cell(cls_2, self.genre_cols).values)

    def delete_cluster(self, cls_2: int) -> None:
        """
        Deletes the requested cluster from the state dataFrame.

        Args:
            cls_2: the cluster id
        """
        self.drop_fields(Settings.prefix_c + str(cls_2), axis=1, inplace=True)
        self.drop_fields(cls_2, axis=0, inplace=True)
        self.update_cluster_cols()

    def update_cluster_cols(self) -> None:
        """
        Updates the current state's cluster list
        """
        self.cluster_cols = add_prefix(self.get_row_names(), Settings.prefix_c)

    def update_distances(self, cls_1: int, centroid: np.ndarray) -> None:
        """
        Updates the distance values corresponding to a given cls_id id and his new centroid

        Args:
            cls_1: cluster id
            centroid: the cluster representation
        """
        centroids = self.get_centroid().values
        matrix = get_distances_centroid_to_centroids(centroid, centroids,
                                                     get_config().minkowski_degree)
        matrix[matrix == 0] = np.nan
        self.update_cell(cls_1, self.cluster_cols, matrix)

    def merge_clusters(self, cls_1: int, cls_2: int,
                       new_index: int, centroid: np.ndarray) -> None:
        """
         Merges the requested clusters into the given new cluster

        Args:
            cls_1: Old Cluster 1 id
            cls_2: Old Cluster 2 id
            new_index: New Cluster id
            centroid: New Cluster's centroid
        """
        self.update_genres(cls_1, cls_2)
        self.delete_cluster(cls_2)
        self.update_distances(cls_1, centroid)
        self.rename({Settings.prefix_c + str(cls_1): Settings.prefix_c + str(new_index)})
        self.rename({cls_1: new_index}, axis=0)
        self.update_cluster_cols()
        self.increase_iteration()

    def update_purity(self) -> None:
        """
        Supervised evaluation of the current state, only for informative purposes.
        """
        self.purity = 100 * sum(
            self.get_sub_data_frame(self.genre_cols).max(axis=1)) / self.initial_num_clusters


class Tree:
    """
    The Hierarchical Clustering Tree or Dendrogram

    Attributes:
        clusters (dict): The initial clusters generated by converting each instance to a cluster
    """

    def __init__(self, initial_clusters: Dict[str, Node]):
        self.clusters = initial_clusters

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def get_instances_clustered(self) -> MyDataFrame:
        """
        Given the current state of the tree it gets the clustered instances
        Returns:
            The container of the {} with the clustered instances' info
        """
        df_instances_clustered = MyDataFrame(
            pd.DataFrame(columns=[Settings.predicted_col, Settings.cluster_col, Settings.info_col]))
        df_instances_clustered.update_index_name(Settings.index_name)
        for cluster in self.get_values():
            cluster.insert_data(df_instances_clustered)
        return df_instances_clustered

    def get_values(self) -> ValuesView[Node]:
        """
        Returns the current state's cluster nodes

        Returns:
            The list of nodes.
        """
        return self.clusters.values()

    def get_keys(self) -> KeysView[str]:
        """
        Returns the current state's cluster id's

        Returns:
            The list of ids
        """
        return self.clusters.keys()

    def extend_tree(self, num_clusters: int) -> None:
        """
        Expands the tree to the requested length
        Args:
            num_clusters:
                The new length of the tree
        """
        while len(self.clusters) < num_clusters:
            left, right = self.clusters.pop(str(max(map(int, self.get_keys())))).get_children()
            self.add_node(left.cls_id, left)
            self.add_node(right.cls_id, right)

    def get_model(self) -> KMeans:
        """
        Obtains the clustering model given the current tree's state
        Returns:
            The instance of the generated `modules.models.KMeans`
        """
        arr = []
        for cls in self.get_values():
            row = list(cls.centroid)
            row.insert(0, cls.genre)
            row.append(cls.get_text())
            arr.append(row)
        return KMeans(arr)

    def get_instances_ids(self, cls: Union[str, int]) -> List[int]:
        """
        Obtains the corresponding instances id grouped in the requested cluster

        Args:
            cls: The requested cluster id

        Returns:
            A list with the instances' ids.
        """
        instances = self.clusters[str(cls)].get_instances_ids()
        if isinstance(instances, int):
            return [instances]
        else:
            return instances

    def get_assigned_cluster(self, instance_id: int) -> int:
        """
        Given a instance id it obtains the associated cluster_id.

        Args:
            instance_id: The id of the instance

        Returns:
            The cluster id
        """
        for cluster in self.get_keys():
            instances = self.get_instances_ids(cluster)
            if instance_id in instances:
                return int(cluster)
        raise ValueError()

    def pop_node(self, cls_id: int) -> Node:
        """
        Pops the requested node from the current state

        Args:
            cls_id: The cluster id

        Returns:
            The associated node to the cluster
        """
        return self.clusters.pop(str(cls_id))

    def add_node(self, cls_id: Union[str, int], node: Node) -> None:
        """
        Adds a new node to the current state

        Args:
            cls_id: id of the new cluster
            node: the reference to the cluster node info:
        """
        self.clusters.update({str(cls_id): node})

    def merge_nodes(self, cls_1: int, cls_2: int,
                    new_cls_id: int, new_centroid: np.ndarray, genre: str) -> None:
        """
        Merges the requested nodes into the new node

        Args:
            cls_1: cluster 1 id
            cls_2: cluster 2 id
            new_cls_id: new cluster id
            new_centroid: new cluster's centroid value
            genre: new cluster's most frequent genre
        """
        cls_node = ClusterNode(new_cls_id, new_centroid, genre, self.pop_node(cls_1),
                               self.pop_node(cls_2))
        self.add_node(new_cls_id, cls_node)

    def get_current_clusters(self) -> str:
        """
        Gets the string representation of the iteration state in the clusters and instances aspect

        Returns:
            The string representation
        """
        return ','.join([str(cluster.get_instances_ids()) for cluster in self.get_values()])


class HierarchicalClustering:
    """
    Handles the training of the clustering model.
    Attributes:
        tree (Tree): The dendrogram of the clustering process.
        info (modules.evaluation.InfoDataFrame): The historical of the process.
        original (ClusteringIteration): The original state of the process.
        df_state (ClusteringIteration): The current state of the process.
        history (List[Union[int,float]]): The list containing all the merges throughout the process.
        """

    def __init__(self, df_state: ClusteringIteration, clusters: Dict[str, Node]):
        self.tree = Tree(clusters)
        self.info = InfoDataFrame()
        self.original = ClusteringIteration(df_state.get_copy())
        df_state.rename(dict(zip(self.original.get_row_names(), range(0, len(self.original)))),
                        axis=0)
        df_state.rename(
            dict(zip(add_prefix(self.original.get_row_names(), Settings.prefix_c),
                     add_prefix(range(0, len(self.original)), Settings.prefix_c))))
        df_state.update_cluster_cols()
        self.df_state = df_state
        self.history = []

    def dendrogram(self, output_folder: str = None) -> None:
        """
        Plots the clustering history into a dendrogram

        Args:
            output_folder: if it's not none it will save the dendrogram in that location
        """
        plt.figure(figsize=(10, 7))
        plt.title('Hierarchical Clustering Dendrogram')
        dendrogram(np.array(self.history, dtype=float),
                   labels=self.original.get_row_names(),
                   orientation="top",
                   distance_sort="descending",
                   show_leaf_counts=True, p=5, truncate_mode="level")
        if output_folder is not None:
            plt.savefig(output_folder + "dendrogram.png")
        else:
            plt.show()
        plt.close()

    def update_centroid(self, cls_1: int, cls_2: int) -> np.ndarray:
        """
         Integrates the cluster2 representation into the cluster 1

        Args:
            cls_1: cluster 1 id
            cls_2: cluster 2 id

        Returns:
            the generated new representation
        """
        num_instances_1 = self.df_state.get_num_instances_in_cluster(cls_1)
        num_instances_2 = self.df_state.get_num_instances_in_cluster(cls_2)
        centroid = (self.df_state.get_centroid(
            cls_1).values * num_instances_1 + self.df_state.get_centroid(
            cls_2).values * num_instances_2) / (num_instances_1 + num_instances_2)
        self.df_state.update_cell(cls_1, Settings.n_instances, num_instances_1 + num_instances_2)
        self.df_state.update_centroid(cls_1, centroid)
        return centroid

    def unique_link(self, cluster: int,
                    single_link: bool = True) -> Tuple[float, Tuple[int, int]]:
        """
        Performs a single or complete linkage into the requested cluster

        Args:
            cluster: The cluster id
            single_link: If false complete linkage

        Returns:
            The min distance and the 2 most near clusters
        """
        instances = self.tree.get_instances_ids(cluster)
        df_distances = self.original.get_cell(instances, self.original.cluster_cols)
        df_distances = df_distances.drop(add_prefix(instances, Settings.prefix_c), axis=1)
        if single_link:
            series = df_distances.min(axis=1)
            min_distance = series.min()
            return min_distance, (
                cluster, rm_prefix(df_distances.idxmin(axis=1).iloc[series.argmin()]))
        else:
            series = df_distances.max(axis=1)
            min_distance = series.max()
            return min_distance, (
                cluster, rm_prefix(df_distances.idxmax(axis=1).iloc[series.argmax()]))

    def intra_cluster(self, instances: List[int]) -> List[float]:
        """
        It performs the Silhouette's intra cluster evaluation

        Args:
            instances: List of instances in a given cluster

        Returns:
            The evaluation value
        """
        if len(instances) == 1:
            return [0.0]
        else:
            return self.original.get_cell(
                instances, add_prefix(instances, Settings.prefix_c)).mean(axis=1).values

    def inter_cluster(self, instances: List[int], cluster: str) -> List[float]:
        """
        It performs the Silhouette's inter cluster evaluation

        Args:
            instances: list of instances in a given cluster
            cluster: cluster id

        Returns:
            The evaluation value
        """
        i_centroids = self.original.get_centroid(instances).append(
            self.df_state.get_sub_data_frame(self.df_state.vector_cols).drop(cluster, axis=0))
        matrix = np.asarray(
            [get_distances_centroid_to_centroids(i_centroids.values[i], i_centroids.values,
                                                 get_config().minkowski_degree) for i in
             range(0, len(instances))])
        matrix[matrix == 0] = np.nan
        df_distances = pd.DataFrame(matrix, index=instances,
                                    columns=add_prefix(i_centroids.index.values, Settings.prefix_c))
        return df_distances.drop(add_prefix(instances, Settings.prefix_c), axis=1).min(
            axis=1).values

    def silhouette(self) -> float:
        """
        It performs the Silhouette Index evaluation into the clusters

        Returns:
            The evaluation value

        """
        aux = 0
        for cluster in self.df_state.get_row_names():
            instances = self.tree.get_instances_ids(cluster)
            a_values = self.intra_cluster(instances)
            b_values = self.inter_cluster(instances, cluster)
            for a, b in zip(a_values, b_values):
                aux += (b - a) / max(a, b)
        return aux / len(self.original)

    def calinski_harabasz_score(self) -> float:
        """
        It performs the Calinski Harabasz evaluation into the clusters
        Returns:
             The evaluation value
        """
        mean = self.df_state.get_centroid().mean(axis=0).values
        inter_index, intra_index = 0., 0.
        for cluster in self.tree.get_keys():
            instances = self.tree.get_instances_ids(cluster)
            cluster_k = self.original.get_centroid(instances)
            mean_k = cluster_k.mean(axis=0).values
            inter_index += len(cluster_k) * np.sum((mean_k - mean) ** 2)
            intra_index += np.sum((cluster_k.values - mean_k) ** 2)
        k = len(self.df_state.dataFrame)
        n = len(self.original.dataFrame)
        return (1. if intra_index == 0. or k == 1 else
                inter_index * (n - k) /
                (intra_index * (k - 1.)))

    def evaluate_clusters(self) -> None:
        """
        Performs a evaluation info the clusters depending the program configuration.
        """
        self.df_state.evaluator_metric = 0
        if get_config().evaluation_metric:
            self.df_state.evaluator_metric = self.calinski_harabasz_score()
        else:
            self.df_state.evaluator_metric = self.silhouette()

    def centroid_linkage(self) -> Tuple[float, int, int]:
        """
        Gets the 2 most near clusters by the centroid linkage metric a

        Returns
            The min distance and the clusters' ids
        """
        cls_1 = self.df_state.get_sub_data_frame(self.df_state.cluster_cols).min(axis=0).idxmin()
        cls_2 = self.df_state.get_sub_data_frame(cls_1).idxmin()
        return float(self.df_state.get_cell(cls_2, cls_1)), rm_prefix(cls_1), int(cls_2)

    def single_complete_link(self, single: bool = True) -> Tuple[float, int, int]:
        """
        Gets the 2 most near clusters by the single or complete linkage metric

        Args:
            single: if False complete linkage

        Returns:
            The min distance and the clusters' ids
        """
        distance, clusters = min(
            self.df_state.apply_to_all_rows(lambda cluster: self.unique_link(cluster, single)),
            key=lambda t: t[0])
        return distance, clusters[0], self.tree.get_assigned_cluster(clusters[1])

    def mean_link(self) -> Tuple[float, int, int]:
        """
        Gets the 2 most near clusters by the mean linkage metric

        Returns:
            The min distance and the clusters' ids
        """
        mean = 9999
        cls_1 = 0
        cls_2 = 0
        clusters = self.df_state.dataFrame.index.tolist()
        rows = [self.tree.get_instances_ids(cls) for cls in clusters]
        cols = list(map(lambda x: add_prefix(x, Settings.prefix_c), rows))
        for i, row in enumerate(rows[:-1]):
            cols = cols[1:]
            series = self.original.dataFrame.loc[row, :]
            for j, col in enumerate(cols):
                distance = series[col].mean(axis=1).mean()
                if distance < mean:
                    cls_1 = clusters[i]
                    cls_2 = clusters[j + i + 1]
                    mean = distance
        return mean, cls_1, cls_2

    def get_most_near_clusters(self) -> Tuple[int, int]:
        """
        By the `Config.linkage_metric` configured linkage metric gets the 2 most near clusters

        Returns:
             The clusters' ids
        """
        linkage = get_config().linkage
        if linkage == 3:
            distance, cls_1, cls_2 = self.centroid_linkage()
        elif linkage == 2 or linkage == 1:
            distance, cls_1, cls_2 = self.single_complete_link(linkage == 2)
        else:
            distance, cls_1, cls_2 = self.mean_link()
        self.df_state.min_distance = distance
        return cls_1, cls_2

    def cluster(self) -> None:
        """
        Performs a clustering iteration
        """
        cls_1, cls_2 = self.get_most_near_clusters()
        new_cls = self.df_state.get_new_cluster_index()
        centroid = self.update_centroid(cls_1, cls_2)
        self.df_state.merge_clusters(cls_1, cls_2, new_cls, centroid)
        self.tree.merge_nodes(cls_1, cls_2, new_cls, centroid, self.df_state.get_genre([new_cls]))
        if len(self.df_state) < get_config().max_num_clusters:
            self.evaluate_clusters()
            self.df_state.update_purity()
        self.save_iteration()
        self.add_history(cls_1, cls_2, new_cls)

    def add_history(self, cls_1: int, cls_2: int, new_cls: int):
        """
        Records the merge for dendrogram displaying purposes

        Args:
            cls_1: old cluster 1 id
            cls_2: old cluster 2 id
            new_cls: new cluster id
        """
        linkage = get_config().linkage
        if linkage == 2 or linkage == 1:
            distance = self.df_state.min_distance
        else:
            distance = self.df_state.iteration
        self.history.append(
            [cls_1, cls_2, distance, self.df_state.get_num_instances_in_cluster(new_cls)])

    def clustering(self, output_folder: str):
        """
        Performs the training of the hierarchical clustering model and saves the generated files.

        Args:
            output_folder: location of where the generated files will be saved
        """
        while len(self.df_state) > 1:
            self.cluster()
        self.info.save_data_frame(output_folder + "info.csv")
        self.tree.extend_tree(self.info.get_num_clusters())
        df = self.original.drop_fields(self.original.cluster_cols, axis=1, inplace=False)
        df.drop_fields(Settings.n_instances, axis=1, inplace=True)
        df.merge(self.tree.get_instances_clustered(), inplace=True)
        df.update_index_name(Settings.index_name)
        df.save_data_frame(output_folder + "train_clustered.csv")
        model = self.tree.get_model()
        model.save_model(output_folder + "model.pkl")
        if get_config().save_graphs:
            self.save_plots(output_folder)

    def save_plots(self, output_folder: str) -> None:
        """
        Generates the corresponding evaluations plots and the dendrogram

        Args:
            output_folder: folder where the plots will be saved
        """
        self.info.plot(col=Settings.evaluation, output_folder=output_folder)
        self.info.plot(col=Settings.accuracy, output_folder=output_folder)
        try:
            self.dendrogram(output_folder)
        except RecursionError:
            pass

    def save_iteration(self) -> None:
        """
        Saves the current iteration info
        """
        row = [round(self.df_state.min_distance, 3),
               self.tree.get_current_clusters(),
               len(self.df_state), self.df_state.evaluator_metric, self.df_state.purity]
        self.info.add_element(row)
