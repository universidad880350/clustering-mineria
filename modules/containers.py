from typing import Mapping, Callable, Sequence, Optional, Union, Any, Dict, Tuple, List

import numpy as np
import pandas as pd

from modules.documentation import docstring_inherit, docstring_parameter, \
    ExternalLinks
from modules.settings import Settings


@docstring_parameter(ExternalLinks.pandas_data_frame, ExternalLinks.pandas_data_frame.module)
class MyDataFrame:
    """{} container for easier method processing

    Attributes:
        dataFrame ({}): The containing dataset"""

    def __init__(self, data_frame: pd.DataFrame, ):
        self.dataFrame = data_frame

    def __len__(self) -> int:
        """
        Gets the length of the dataFrame
        Returns:
            Length of the dataframe
        """
        return self.dataFrame.__len__()

    def set_dtypes(self, dtypes: Mapping[str, type]) -> None:
        """
        Given a dictionary modifies the contained data_frame's column types
        Args:
            dtypes: Corresponds to the dictionary containing in the key the
            column and in the value the type
        """
        self.dataFrame.astype(dtypes)

    @staticmethod
    def get_data_frame(input_path: str, sep: str = ",") -> 'MyDataFrame':
        """
        Retrieves the dataframe from a given csv path and generates a instance
        Args:
            input_path: path to the csv
            sep: the separator of the csv cols

        Returns:
            The container of the retrieved dataFrame
        """
        data_frame = pd.read_csv(input_path,
                                 sep=sep,
                                 dtype={Settings.index_name: int},
                                 header=0,
                                 index_col=Settings.index_name)
        return MyDataFrame(data_frame)

    def apply_to_all_rows(self, function: Callable):
        """
        Applies a function to all rows of the dataFrame

        Args:
            function: The function that will be applied

        Returns:
            The values obtained by applying the function thorough the rows

        """
        return self.dataFrame.index.map(function).values

    def get_arg_max(self, axis: int = 1):
        """
        gets the arg(s) that maximize over an axis
        Args:
            axis:  0 -> Row 1 -> Column

        Returns:
            The arg that meets the criteria
        """
        return self.dataFrame.idxmax(axis=axis)

    def get_row_names(self) -> np.ndarray:
        """
        Gets the row names of the dataframe
        Returns:
            A list of the id values
        """
        return self.dataFrame.index.values

    def get_max_index(self) -> int:
        """
        Gets the max value of the id col

        Returns: The max index row id.

        """
        if (len(self.dataFrame)) == 0:
            return 0
        return self.dataFrame.index.max()

    def get_col_names(self) -> np.ndarray:
        """
        Gets the names of the columns of the dataframe
        Returns:
            A list of the column names
        """
        return self.dataFrame.columns.values

    def filter(self, prefix: str, axis: int, inplace: bool = False) -> Optional['MyDataFrame']:
        """
        Filters the dataframe throughout an axis and a given prefix.
        Args:
            prefix: The prefix of the items that will be filtered
            axis:  0 -> Rows 1 -> Columns
            inplace: If false returns a copy.
        Returns:
            If inplace is False returns the generated dataFrame.

        """
        if inplace:
            self.dataFrame = self.dataFrame.filter(like=prefix, axis=axis)
        else:
            return MyDataFrame(self.dataFrame.filter(like=prefix, axis=axis))

    def save_data_frame(self, output_path: str, sep: str = ",") -> None:
        """
        Saves the current data frame into a csv
        Args:
            output_path: Path of where is the csv going to be generated
            sep: Sets the separator for the csv

        """
        self.dataFrame.to_csv(output_path, sep=sep)

    def to_pickle(self, output_path: str) -> None:
        """
        Saves the current data frame into a pickle file.
        Args:
            output_path: Path of where is the file going to be generated
        """
        self.dataFrame.to_pickle(output_path)

    @staticmethod
    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def from_pickle(input_path: str) -> 'MyDataFrame':
        """
        Given a pickle file and program settings it creates a `MyDataFrame` instance

        Args:
            input_path: path of where is the csv going to be extracted from

        Returns: An instance of the container `MyDataFrame` with it's corresponding {}

        """
        return MyDataFrame(pd.read_pickle(input_path))

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def get_subset(self, percentage: float, drop: bool = False) -> 'MyDataFrame':
        """
        It returns a new container with a subset of the {},
        if drop is set to True it removes the generated subset in the instance.
        Args:
            percentage: It corresponds to the relative size of the subset
            drop: If the instance should pop or get the subset
        Returns:
            The container of the subset
        """
        my_data_frame = MyDataFrame(self.dataFrame.sample(frac=percentage))
        if drop:
            self.drop_fields(my_data_frame.dataFrame.index, axis=0)
        return my_data_frame

    @docstring_parameter(dataframe=ExternalLinks.pandas_data_frame,
                         series=ExternalLinks.pandas_series)
    def get_cell(self, row: Union[int, Sequence[int]],
                 col: Union[str, Sequence[str]]) -> Union[Any, pd.Series, pd.DataFrame]:
        """
        Given row(s) and col(s) it returns the resulting cell in {dataframe} or {series} formats.
        Args:
            row: Row(s) of the dataFrame
            col: Cols(s) of the dataFrame

        Returns:
            The resulting {dataframe} or {series}.
        """
        return self.dataFrame.loc[row, col]

    def update_cell(self, row: Union[int, Sequence[int]], col: Union[str, Sequence[str]],
                    value) -> None:
        """
        It updates the requested cell with new values
        Args:
            row: the corresponding row(s) of the cell
            col: the corresponding col(s) of the cell
            value: the new value of the cell
        """
        self.dataFrame.at[row, col] = value

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def update_data_frame(self, new_data_frame: pd.DataFrame) -> None:
        """
        Updates the container's dataframe to the requested {}.
        Args:
            new_data_frame: The requested new dataFrame
        """
        self.dataFrame = new_data_frame

    @docstring_parameter(dataframe=ExternalLinks.pandas_data_frame,
                         series=ExternalLinks.pandas_series)
    def get_sub_data_frame(self,
                           fields: Union[str, int, Sequence[Union[str, int]]]) -> \
            Union[pd.Series, pd.DataFrame]:
        """
        Given a row(s) or col(s) index it returns it's corresponding {series} or sub
        {dataframe}.
        Args:
            fields: name(s) of the index
        Returns:
            The corresponding series or sub dataFrame
        """
        try:
            return self.dataFrame[fields]
        except KeyError:
            print(self.dataFrame)
            quit()

    @docstring_parameter(dataframe=ExternalLinks.pandas_data_frame,
                         series=ExternalLinks.pandas_series)
    def update_sub_data_frame(self, fields: Union[str, int, Sequence[Union[str, int]]],
                              values: Any) -> None:
        """
        Given a row(s) or col(s) index it updates it's corresponding {series} or sub
        {dataframe}.
        Args:
            fields: name(s) of the index
            values: the new value
        """

        self.dataFrame[fields] = values

    @docstring_parameter(dataframe=ExternalLinks.pandas_data_frame)
    def drop_fields(self, fields: Union[str, int, Sequence[Union[str, int]]],
                    axis: int, inplace: bool = True) -> Optional['MyDataFrame']:
        """
        Given a list of index it makes a copy and removes them and or removes them in place
        Args:
            axis: Represents the dimension of the fields. 0 -> Rows. 1-> Cols
            fields: The list of index axis: The axis of the index
            inplace:  If false it returns the generated {dataframe}

        Returns:
            A MyDataFrame container with the generated {dataframe} if inplace is set to True.
        """
        if inplace:
            self.dataFrame.drop(fields, axis, inplace=inplace)
        else:
            return MyDataFrame(self.dataFrame.drop(fields, axis, inplace=inplace))

    @docstring_parameter(dataframe=ExternalLinks.pandas_data_frame)
    def merge(self, df_to_merge: 'MyDataFrame', inplace: bool = True) -> Optional['MyDataFrame']:
        """
        Merges 2 `MyDataFrame` instances, updating it or returning it depending the inplace.

        Args:
            df_to_merge: The {dataframe} container to be merged
            inplace: if True updates the current instance with the generated {dataframe}.

        Returns:
            If inplace is False returns the `MyDataFrame` instance.

        """
        if inplace:
            self.dataFrame = self.dataFrame.merge(df_to_merge.dataFrame, right_index=True,
                                                  left_index=True)
        else:
            return MyDataFrame(
                self.dataFrame.merge(df_to_merge.dataFrame, right_index=True, left_index=True))

    def drop_na(self) -> None:
        """
        It removes the rows and cols that contain np.nan values
        """
        self.dataFrame.dropna(inplace=True)

    def rename(self, renaming_index: Dict[Union[str, int], Union[str, int]], inplace: bool = True,
               axis: int = 1) -> Optional['MyDataFrame']:
        """
        Given a dictionary with the old names in keys and the new names in values it renames the
        index.
        Args:
            renaming_index: The dictionary with the requested changes {old_name : new_name}
            inplace:  If true it updates it in place. Else, it returns the generated
            `MyDataFrame`
            axis: it corresponds to col if 1 and row if 0

        Returns:
            If inplace is False returns the generated `MyDataFrame` container.
        """
        if inplace:
            self.dataFrame.rename(renaming_index, inplace=inplace, axis=axis)
        else:
            return MyDataFrame(self.dataFrame.rename(renaming_index, inplace=inplace, axis=axis))

    def print(self, num_rows: int = 5) -> None:
        """
        Prints the n first rows of the current dataFrame

        Args:
            num_rows: Quantity of rows requested to be printed
        """
        print(self.dataFrame.head(num_rows))

    def get_copy(self) -> 'MyDataFrame':
        """
        It returns a copy of the container with the same settings
        Returns:
            The generated `MyDataFrame` copy

        """
        return MyDataFrame(self.dataFrame.copy())

    def update_index_name(self, name: str) -> None:
        """
        Updates the col name corresponding to the index

        Args:
            name: The new index col name
        """
        self.dataFrame.index.name = name

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def add_row(self, index: Union[int, Sequence[int]], value: Sequence[Any]) -> None:
        """
        Adds a new row to the dataFrame

        Args: index: the New Row indexes value:  A list with the values of the row matching the
        {} cols

        """

        self.dataFrame.loc[index] = value


class Node:
    """
    Abstract class for reducing redundant src of `InstanceNode` and `ClusterNode`

    Attributes:
        cls_id (int): Corresponds to the cluster id
        centroid (np.ndarray): The Vec Representation of the `Node`
        genre (str): The most frequent genre in the `Node`
    """

    def __init__(self, cls_id: int, centroid: np.ndarray, genre: str):
        self.cls_id = cls_id
        self.centroid = centroid
        self.genre = genre

    def get_instances(self) -> None:
        """
        Abstract method that must be on the children classes

        Raises:
            NonImplementedError: If method not implemented in inherited classes.
        """
        raise NotImplementedError()

    def get_text(self) -> None:
        """
        Abstract method that must be on the children classes

        Raises:
            NonImplementedError: If method not implemented in inherited classes.
        """
        raise NotImplementedError()

    def get_instances_ids(self) -> Optional[Union[int, List[int]]]:
        """
        Abstract method that must be on the children classes

        Raises:
            NonImplementedError: If method not implemented in inherited classes.
        """
        raise NotImplementedError()

    def insert_data(self, *args, **kwargs):
        raise NotImplementedError()


@docstring_inherit(Node)
class InstanceNode(Node):
    """
    `Node` of the `modules.clustering.Tree` corresponding to a instance. It represents a leaf on
    the tree.

    Attributes:
        text (str): It corresponds to the vec representation of the instance's overview
        instance_id (int): It corresponds to the id of the instance
        film_name (str): It corresponds to the name of the instance's film

    """

    def __init__(self, cls_id: int, centroid: np.ndarray, instance_id: int, real_genre: str,
                 overview: str,
                 film_name: str):
        super().__init__(cls_id, centroid, real_genre)
        self.instance_id = instance_id
        self.text = overview
        self.film_name = film_name

    def get_instances(self) -> Dict[int, 'InstanceNode']:
        """
        Returns the dictionary with the reference to the leaf by the cls_id id

        Returns:
            The generated dict
        """
        return {self.cls_id: self}

    def get_text(self) -> str:
        """
        Returns a text containing the film name and the overview

        Returns:
            The generated string
        """

        return " Film:\t{}\n Plot: {}".format(self.film_name, self.text)

    def get_instances_ids(self) -> int:
        """
        Returns the id of the instance

        Returns:
            The id of the instance

        """
        return self.instance_id

    @docstring_parameter(ExternalLinks.pandas_data_frame)
    def insert_data(self, my_data_frame: MyDataFrame,
                    predicted_genre: str = None, cls_id: int = None,
                    text: str = None) -> None:
        """
        Given a dataFrame inserts the leaf data, depending if the leaf itself if is one of
        the chosen clusters or no it will insert his own cls_id data or it's parents.
        Args:
            my_data_frame: It corresponds to the generated {} by adding chosen clusters info
            predicted_genre: The most frequent genre in the cluster
            cls_id: The cls_id identification
            text: A text with films and texts of the same cluster
        """
        if predicted_genre is None:
            predicted_genre = self.genre
        if cls_id is None:
            cls_id = self.cls_id
        if text is None:
            text = self.get_text()
        my_data_frame.add_row(int(self.instance_id), [predicted_genre, cls_id, text])


@docstring_inherit(Node)
class ClusterNode(Node):
    """
    It represents a cluster `Node` of the `modules.clustering.Tree`.
    Attributes:
        instances (Dict[int,Node]): It corresponds to the instances in the cluster.
        left (Node): It corresponds to the left `Node`.
        right (Node): It corresponds to the right `Node`.
    """

    def __init__(self, cls_id: int, centroid: np.ndarray, genre: str, left: Node, right: Node):
        super().__init__(cls_id, centroid, genre)
        self.left = left
        self.right = right
        self.instances = {**left.get_instances(), **right.get_instances()}

    def get_text(self) -> str:
        """
        It generates a guide text with some films of the cluster and with theirs plots.
        Returns:
           The generated string
        """
        return "\n ".join([instance.get_text() for instance in list(self.instances.values())[:5]])

    def get_children(self) -> Tuple[Node, Node]:
        """
        It returns the children of the current node

        Returns:
            Left and right `Node`
        """
        return self.left, self.right

    def get_instances(self) -> Dict[int, InstanceNode]:
        """
        It returns the dictionary of the references to the `InstanceNode` contained in the clusters

        Returns:
            The references dictionary
        """
        return self.instances

    def get_instances_ids(self) -> List[int]:
        """
        It returns the list of the id of the instances on the clusters

        Returns:
            List of ids
        """
        return [instance_node.get_instances_ids() for instance_node in self.instances.values()]

    def insert_data(self, my_data_frame: MyDataFrame) -> None:
        """
        Inserts the clustered instances info to the given dataframe in construction.

        Args:
            my_data_frame: The dataframe to be updated
        """
        for instance in self.instances.values():
            instance.insert_data(my_data_frame, self.genre, self.cls_id, self.get_text())
