from argparse import Namespace, Action, ArgumentParser, RawTextHelpFormatter
from pickle import load, dump
from sys import argv
from typing import Any, Tuple, List, Optional

import modules


class Settings:
    """
    Global settings of the column names
    """
    index_name = "id"  # index name for all data frames
    description_col = "overview"  # column name for description
    modified_col = "modified"
    film_col = "original_title"
    n_instances = "num_instances"  # column name for num instances in cluster
    types = "genres"  # column name for genres
    language = "original_language"  # column name for language
    prefix_t = "genre_"  # prefix for genre columns
    prefix_v = "vec_"  # prefix for vector elements
    prefix_c = "cluster_"  # prefix for cluster elements
    distance_metric = "minkowski"
    genre_col = "genre"  # column name for genre
    info_col = "info"  # column name for info
    accuracy = "purity"  # column name for accuracy
    evaluation = "evaluation"  # column name for evaluation
    predicted_col = "predicted_genre"  # column name for predicted
    cluster_col = "cls_id"  # column name for cluster


class Config:
    """
    It handles the settings of the program and the process of parsing the arguments.

    Attributes:
        vector_size (int): vector size of the embeddings
        window (int): num elements taking into account vectoring a word
        minkowski_degree (int): distance minkowski degree
        evaluation_metric (bool): Calinski False -> Silhouette
        linkage (int): 0 -> Mean; 1 -> Single; 2-> Complete; 3->Average
        spacy_model (str): "en_core_web_md"
        choose_num_clusters (bool): True -> Manually False -> Automatically
        max_num_clusters (int): Max num of clusters
        save_graphs (bool): True -> Save Graphs
        min_num_clusters (int): Min num of clusters
    """

    def __init__(self):
        self.vector_size = 500
        self.window = 3
        self.minkowski_degree = 2
        self.evaluation_metric = True
        self.linkage = 3
        self.spacy_model = "en_core_web_md"
        self.choose_num_clusters = False
        self.max_num_clusters = 200
        self.save_graphs = True
        self.min_num_clusters = 50

    @staticmethod
    def load_config(input_path: str) -> 'Config':
        """
        Given a input config file it returns the `Config` instance with the requested settings.

        Args:
            input_path: It corresponds to the path where the config file is stored

        Returns:
            A `Config` instance with the settings set on the input file
        """
        with open(input_path, "rb") as f:
            config = load(f)
            f.close()
        return config

    def modify_attrs(self, attrs: List[Tuple[str, Any]]) -> None:
        """
        Given a array of modifications it modifies the Config instance settings matching
        the requested settings.

        Args:
            attrs: Array of tuples with the format (attr_name, attr_value)
        """
        for attr, value in attrs:
            self.set_attr(attr, value)

    def save_config(self, output_path: str) -> None:
        """
        Saves the Config instance settings into the output file

        Attributes:
            output_path: It corresponds to the path to where the instance will be stored
        """
        with open(output_path, "wb") as f:
            dump(self, f, -1)
            f.close()

    @staticmethod
    def handle_parser(help_message: str, has_input: bool = True, has_output: bool = True):
        """
        It generates the program arguments with the default parser and a given help message

        Args:
            help_message: It corresponds to the help of the program
            has_input: It corresponds if the default program needs an input
            has_output: It corresponds if the default program needs an output

        Returns:
            It returns the processed args in Namespace format

        """
        # TODO link a namespace
        return Config.get_args(Config.get_parser(help_message, has_input, has_output))

    @staticmethod
    def get_args(parser: ArgumentParser) -> Namespace:
        """
        It generates the program arguments given a parser.

        Args:
            parser: It corresponds to the parser

        Returns:
            It returns the processed args in `Namespace` format

        """
        arguments = parser.parse_args(argv[1:])
        modules.init(Config.handle_config_args(arguments))
        return arguments

    @staticmethod
    def get_parser(help_message: str, has_input: bool = True,
                   has_output: bool = True) -> Optional[ArgumentParser]:
        """
        It generates the default parser for the program.

        Args:
            help_message: It corresponds to the default help of the program
            has_input: It corresponds if the program requires an default input
            has_output: It corresponds if the program requires an default output

        Returns:
            It returns the parser `ArgumentParser` with the default configuration
        """
        if len(argv) == 1:
            print_help_message(help_message)
            quit()
        else:
            return Config.config_parser(program_name=argv[0], des=help_message, has_input=has_input,
                                        has_output=has_output)

    @staticmethod
    def config_parser(program_name: str, epilog: str = "", des: str = "", has_input: bool = True,
                      has_output: bool = True) -> ArgumentParser:
        """
        It generates the default parser given the program name and optional settings.

        Args:
            program_name: It corresponds to the name of the current executed script
            epilog: It corresponds to additional info of the help
            des: It corresponds to additional info of the help
            has_input: It states if the program requires an input argument
            has_output: It states if the program requires an output argument

        Returns:
            The `ArgumentParser` object with the default configuration
        """
        # noinspection PyTypeChecker
        config_parser = ArgumentParser(prog=program_name, formatter_class=RawTextHelpFormatter,
                                       epilog=epilog, description=des)
        config_parser.add_argument("--config", "-c", nargs=2,
                                   metavar=("<attr_name>", "<attr_value>"), dest="attrs",
                                   help=Config.help(), required=False,
                                   action=ActionTuple)
        config_parser.add_argument("--save_config", "-s", nargs=1, metavar="<output_path>",
                                   dest="save", required=False,
                                   type=str)
        config_parser.add_argument("--load_config", "-l", nargs=1, metavar="<input_path>",
                                   dest="load", required=False,
                                   type=str)
        if has_input:
            config_parser.add_argument("-i", "--input", nargs=1, dest="input", type=str,
                                       metavar="<input_path>",
                                       required=True)
        if has_output:
            config_parser.add_argument("-o", "--output", nargs=1, dest="output", type=str,
                                       metavar="<output_path>",
                                       required=True)
        return config_parser

    @staticmethod
    def handle_config_args(namespace: Namespace) -> 'Config':
        """
        It holds the job of processing the config scope args:

            - If a config file is given it is loaded if not it creates a default instance of Config.
            - Modifies it's attributes if requested.
            - If a output config file is given it saves the instance.

        Attributes:
            namespace: It corresponds to the program processed args.

        Returns:
             The requested Config Instance.
        """
        if namespace.load is not None:
            config = Config.load_config(namespace.load)
        else:
            config = Config()
            if namespace.attrs is not None:
                config.modify_attrs(namespace.attrs)
            if namespace.save is not None:
                config.save_config(namespace.save)
        return config

    @staticmethod
    def help() -> str:
        """
        It returns the information about the `Config` attributes.
        Returns:
             A string containing the info chart.
        """
        return '''
    attr                default       options     description 
vector_size         500             int         # vector size of the embeddings 
window              3               int         # num elements taking into account vectoring a word 
minkowski_degree    2               int         # distance minkowski degree 
evaluation_metric   True            True/False  # True -> Calinski False -> Silhouette 
linkage             3               0/1/2/3     # 0 -> Mean; 1 -> Single; 2-> Complete; 3->Average 
spacy_model         "en_core_web_md"            # models for spacy https://spacy.io/usage/models 
choose_num_clusters False           True/False  # True -> Manually False -> Automatically 
max_num_clusters    200             int         # maximum num of clusters allowed 
min_num_clusters    50              int         # minimum num of clusters allowed 
save_graphs         True            True/False  # True -> Save Graphs  False -> Dont 
        '''

    def set_attr(self, key: str, value: Any) -> None:
        """
        It modifies the `Config` instance attribute given the name and the new value

        Args:
            key: The attribute name
            value: The new attribute value

        Raises:
            ValueError: If the attribute name doesnt exit.
        """
        old_value = self.__getattribute__(key)
        attr_type = type(old_value)
        try:
            self.__setattr__(key, attr_type(value))
        except ValueError:
            print("It has not update the value because the new value is not valid:")
            print("Attribute {} type: {} value: {}".format(key, attr_type, old_value))


class ActionTuple(Action):
    """
    It handles the process of processing parameters that require 2 values
    """

    def __call__(self, parser: ArgumentParser, namespace: Namespace, values: Tuple[str, Any],
                 option_string: str = None):
        value = namespace.__getattribute__(self.dest)
        new_value = (values[0], values[1])
        if value is None:
            namespace.__setattr__(self.dest, [new_value])
        else:
            value.append((values[0], values[1]))


def print_help_message(help_message):
    """
    prints the help message
    Attributes:
         help_message: the string containing the help text
    """
    print('''
        --------------------------------
        |     Help Default Message     |
        --------------------------------
    ''')
    print(help_message)
