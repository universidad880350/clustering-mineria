#!/usr/bin/env python
from typing import Optional

from modules.settings import Config

__config__ = None


# TODO singleton with the configuration

def init(config: Config) -> None:
    """
    Init of the module with the config file
    Args:
        config: The program settings
    """
    global __config__
    __config__ = config


def get_config() -> Optional[Config]:
    """
    Get the program settings
    Returns:
        The program settings
    """
    return __config__
