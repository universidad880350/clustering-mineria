# [Clustering Data Mining](https://gitlab.com/universidad880350/clustering-mineria)
The main objective of this software is the generation of a useful clustering model that groups the similar films. With
that goal in mind, we are using a given [dataset](https://www.kaggle.com/stephanerappeneau/350-000-movies-from-themoviedborg/notebooks)
that contains the necessary data to accomplish this process. \
The software is divided in different steps:
## Getting started
Setup virtualenv and install libraries
```shell script
sudo apt install virtualenv && python3-pip
virtualenv --python=/usr/bin/python3.6 venv/
source venv/bin/activate
pip3 install -r requirements.txt
python3.6 setup.py install
```
#### Get documentation (requires [pdoc3](https://pypi.org/project/pdoc3/))
```shell script
pdoc3 --html --force modules/
```

## Preprocess Raw File
Filters the missing rows and the rows that original language is not english (as the data is huge better to ensure that
the overview is in english). Splits the genres column into a 
#### Extracted data from the [Raw File](resources/archive/AllMoviesDetailsCleaned.csv)
| Original Title | Overview | Genres | Original_Language |
| :---:  | :--- | :---:  | :---:  |
| Ariel  Taisto  | Kasurinen is a Finnish coal miner whose...  | Drama & Crime   | fi  |
| Four Rooms  | It's Ted the Bellhop's first night on the job....  | Crime & Comedy  | en  |
#### Script used for preprocess the raw file
```shell script
python3.6 bin/raw_to_preprocessed.py -i resources/archive/AllMoviesDetailsCleaned.csv -o resources/out/full_dataset/preprocessed.csv
```
#### Preprocessed data [Preprocessed File](resources/out/full_dataset/preprocessed.csv)
| Original Title | Overview | genre_Crime | ... | genre_War |
| :---:  | :--- | :---:  | :---:  | :---:  |
| Last Summer  | An island with secrets.  | 0 | ...  | 0 |
| Penarek Becha  | The story tells us about a trishaw puller....  | 0 | ...  | 0 |
## Dev and Test
Splits the preprocessed data into a developing purposes dataframe portion, a test portion
and stores the non used instances.
#### Script used for get the samples
```shell script
python3.6 bin/get_sample.py -i resources/out/full_dataset/preprocessed.csv -p 0.03 resources/out/dev/train_preprocessed.csv -p 0.003 resources/out/dev/test_preprocessed.csv -o resources/out/dev/unused_preprocessed.csv
```

#### Generated files
| File | Num Instances | 
| :---: | :---: |
| Dev | 3713 |
| Test  | 360 |
| Unused | 119684 |
## Generate Docs Representation of the Instances
Applies a tokenization the overview of each film, replacing the recognized entities by the set [spacy model](https://spacy.io/usage/models````) and the words
with the lemma.
#### Script used 
```shell script
python3.6 bin/preprocessed_to_docs.py -i resources/out/dev/train_preprocessed.csv -o resources/out/dev/train_docs.csv 
```
#### Generated data from [Docs](resources/out/dev/train_docs.csv)
| id | original_title | overview | genre_Crime | ... | genre_TV Movie | modified |
| :---: | :---: | :--- | :---: | :---: | :---: | :---: |
| 448318 | Last Summer | An island with secrets. | 0 | ... | 0 | \['An', 'island', 'with', 'secret', '.'] |
| 70219 | Penarek Becha | The story tells us abou... | 0 | ... | 0 | \['The', 'story', 'tell', 'us', ...] |

## Generate Vec Representation of the Instances
Applies a vectorization of the overview by training a [Doc2Vec](https://radimrehurek.com/gensim/models/doc2vec.html) model
and returning the resulting representations and the model. Finally, it performs a standardization
of the instances vec representation and generates the resulting Standardise Model.  
#### Script used 
python3.6 doc_to_vec.py -i resources/out/dev/train_docs.csv -o resources/out/
#### Generated files
| File | 
| :---: | 
| [Vectorized Data Frame](resources/out/dev/train_vec.csv) | 
| [Doc2Vec Model](resources/out/dev/doc_to_vec.model) |
| [Train Mean Vector](resources/out/dev/mean.npy) |
| [Train Standard Deviation Vector](resources/out/dev/std.npy) |
#### Vectorized data from [Vec](resources/out/dev/train_vec.csv)
| id | original_title | overview | genre_Crime | ... | vec_498 | vec_499 |
| :---: | :---: | :--- | :---: | :---: | :---: | :---: |
| 292468 | Urs Al-Zayn | Everyone in his village | 0 | ... | -1.238589 | -0.002429 |
| 93767 | Seven Women from Hell | Seven women from different ... | 0 | ... | -0.090666 | 0.608749 |

## Hierarchical Clustering
In order to achieve the training of the hierarchical clusters a few steps are needed beforehand.
### Generate the Distance Matrix of the Clusters
Estimates the distance between all possible instance pairs by minkowski distance and generates
a distance matrix which is merged into the instance dataFrame.
#### Generated DataFrame
| id | genre_Comedy | genre_Action | ... | cluster_27565 | cluster_189813 | num_instances |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 61383 | 0 | 0 | ... | 18.890615 | 10.598184 | 1 |
| 448318 | 0 | 0 | ... | 20.965397 | 24.902719 | 1 |
### Generate the Clusters Tree (Model)
The tree contains the current state of the clustering iteration, being the initial state
where each instance is a node.
### Clusters the nodes
1. Obtains the most near cluster depending the linkage.
2. Merge in the State Data Frame
   - Integrates the 2nd cluster into the 1st cluster.
   - Renames the 1st cluster and removes the 2nd cluster.
3. Merge in the Tree.
   - Pops the 2 cluster nodes.
   - Generates a new cluster node with reference to the popped nodes.
   - Adds the new node to the Tree.
4. Records the cluster's performance into a info data frame.
   - If the num cluster is below a boundary (max_num_clusters) it performs the evaluation
   by Calinski or Silhouette Index.
### After the clustering
1. Get the optimum num clusters
2. Extend the tree to that level.
3. Saves the generated files.

| File | Description | 
| :---: | :--- |
| [Train Clustered](resources/out/dev/train_clustered.csv) | The clustered train instances. |
| [Clustering Info](resources/out/dev/info.csv) | The recorded info about the iterations |
| [Cluster Model](resources/out/dev/model.pkl) | The generated clusterer model |
| [Purity Plot](resources/out/dev/purity.png) | The generated plot with the supervised evaluation for each state |
| [Evaluation Plot](resources/out/dev/evaluation.png) | The generated plot with the unsupervised evaluation for each state |
| [Dendrogram](resources/out/dev/dendrogram.png) | The generated dendrogram |

#### Script used 
```shell script
python3.6 vec_to_clusters.py -i resources/out/dev/train_vec.csv -o resources/out/dev/ 
```
## Cluster New Instances
Clusters the given instances by the previously generated models. 
Options: 

    - f : given a preprocessed data frame returns the clustered data.
    - t : given text files prints the cluster results.
    - else : asks for a manual input and prints the cluster results.
#### Script used 
```shell script
python3.6 bin/cluster_new_instances.py -f resources/out/dev/test_preprocessed.csv resources/out/dev/test_clustered.csv -d resources/out/dev/doc_to_vec.model -m resources/out/dev/model.pkl -e resources/out/dev/mean.npy -std resources/out/dev/std.npy 
```
#### Generated [Clustered Test](resources/out/dev/test_clustered.csv)

## Evaluate the clustered data frame
Performs a supervised evaluation into the clustered dataset.
#### Script used
```shell script
python3.6 bin/supervised_evaluation.py -i resources/out/dev/train_clustered.csv
python3.6 bin/supervised_evaluation.py -i resources/out/dev/test_clustered.csv
```
## Program Settings
As the clustering needs different parameters, the software allows the client to config certain aspects.
#### Aspects that can be modified
| attr | default | options | description | 
| :---: | :---: | :---: | :---: |
| vector_size | 500 | int | vector size of the embeddings |
| window | 3 | int |  num elements taking into account vectoring a word |
| minkowski_degree | 2 | int | distance minkowski degree |
| evaluation_metric | True | True/False | True -> Calinski False -> Silhouette |
| linkage | 3 | 0/1/2/3 | 0 -> Mean; 1 -> Single; 2-> Complete; 3->Average |
| spacy_model | en_core_web_md | str | models for [spacy](https://spacy.io/usage/models) |
| choose_num_clusters | False | True/False | True -> Manually False -> Automatically |
| max_num_clusters | 200 | int | maximum num of clusters allowed |
| min_num_clusters | 50 | int | minimum num of clusters allowed |
| save_graphs | True | True/False | True -> Save Graphs  False -> Dont |
#### Settings handling
| Flag | args | description |
| :---: | :---: | :---: | 
| -h, --help | empty |  Prints the program help | 
| -c, --config | <attr_name> <attr_value> | Configs a program aspect |
| -s, --save_config | <output_path> | Saves the used config into a file |
| -l, --load_config | <input_path> | Loads the program settings |
           
