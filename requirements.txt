setuptools~=50.3.2
matplotlib~=3.3.3
numpy~=1.19.4
spacy~=2.3.2
pandas~=1.1.4
gensim~=3.8.3
scipy~=1.5.4
seaborn~=0.11.0