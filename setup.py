from setuptools import setup, find_packages

setup(
    name='clustering-mineria',
    version='1.1',
    packages=find_packages(exclude=['html', 'tests']),
    url='https://gitlab.com/universidad880350/clustering-mineria',
    license='',
    author='alvaro',
    author_email='alvaro25duenas@gmail.com',
    description='films clustering'
)
