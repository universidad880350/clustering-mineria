from modules.evaluation import SupervisedEvaluation
from modules.settings import Config

help_message = '''
    Usage Example:
        python3.6 supervised_evaluation -i res.csv

    Print Parser Help:
        python3.6 vec_to_clusters -h
'''


def supervised_evaluation(input_path: str):
    """
    Given a clustered instances data frame performs the evaluation taking into account the class.
    :param str input_path: where the input data frame is located
    :param Config config: The settings of the program
    """
    evaluate = SupervisedEvaluation.get_data_frame(input_path)
    print(evaluate.get_confusion_matrix().__str__())
    summary = evaluate.summary()
    print(summary)
    print("Total accuracy of the dataset:\t {}".format(evaluate.evaluate()))


if __name__ == '__main__':
    args = Config.handle_parser(help_message, has_output=False)
    supervised_evaluation(args.input[0])
