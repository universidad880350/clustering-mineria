from modules.clustering import InstancesDataFrame
from modules.settings import Config

help_message = '''
    Usage Example:
        python3.6 doc_to_vec -i clean.csv -o out/
        
    Print Parser Help:
        python3.6 vec_to_clusters -h
'''


def clean_to_vec(input_path: str, output_folder: str):
    """
    It performances the vectorization of the input instances
    :param str input_path: path to the input file
    :param str output_folder: path to where the generated files will be stored
    """
    instances = InstancesDataFrame.get_data_frame(input_path)
    clusters, doc_to_vec, standardization = instances.create_vec_data_frame()
    clusters.save_data_frame(output_folder + "train_vec.csv")
    doc_to_vec.save_model(output_folder + "doc_to_vec.model")
    standardization.save_model(output_folder)


if __name__ == '__main__':
    args = Config.handle_parser(help_message)
    clean_to_vec(args.input[0], args.output[0])
