from modules.containers import MyDataFrame
from modules.settings import Config, ActionTuple

help_message = '''
    Usage Example:
        python get_sample.py -i data.csv -p 0.1 data_test.csv -o data_train.csv 
        
    Print Parser Help:
        python3.6 vec_to_clusters -h
'''


def reduce_file_size(input_file, percentage_tuples_list, output_file):
    my_data_frame = MyDataFrame.get_data_frame(input_file)
    for value, output in percentage_tuples_list:
        subset = my_data_frame.get_subset(float(value), drop=True)
        subset.save_data_frame(output)
    my_data_frame.save_data_frame(output_file)


if __name__ == '__main__':
    parser = Config.get_parser(help_message)
    parser.add_argument("-p", "--percentage", nargs=2, dest="percentage",
                        metavar=("<percentage>", "<output_file>"),
                        required=True, action=ActionTuple)
    args = Config.get_args(parser)
    reduce_file_size(args.input[0], args.percentage, args.output[0])
