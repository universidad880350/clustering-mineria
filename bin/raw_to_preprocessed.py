from modules.clustering import UnprocessedDataFrame
from modules.settings import Config

help_message = '''
    Usage Example:
        python3.6 raw_to_preprocessed.py -i AllMoviesDetailsCleaned.csv -o preprocessed.csv
        
    Print Parser Help:
        python3.6 vec_to_clusters -h
'''


def preprocess_raw(raw_path: str, output_path: str):
    """
    Given the films details dataset it filters the missing values and the non english films and
    saves the generated dataset on the output file.
    :param str raw_path: path to the AllDetailsCleaned.csv
    :param str output_path: path to where the preprocessed csv is requested to be stored
    """
    unprocessed_data_frame = UnprocessedDataFrame.get_data_frame(raw_path, sep=";")
    processed_data_frame = unprocessed_data_frame.preprocess()
    processed_data_frame.save_data_frame(output_path)


if __name__ == '__main__':
    args = Config.handle_parser(help_message)
    preprocess_raw(args.input[0], args.output[0])
