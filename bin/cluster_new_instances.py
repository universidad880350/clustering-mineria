from modules.models import DocToVec, Standardization, KMeans, Clusterer
from modules.containers import MyDataFrame
from modules.settings import Config, ActionTuple

help_message = '''
    Usage Example:
        python3.6 cluster_new_instances -m model.pkl -d doc_to_vec.model -e mean.npy -std std.npy

    Print Parser Help:
        python3.6 vec_to_clusters -h
'''


def cluster_new_instances(tuples, texts, model_path, doc_to_vec, mean_path, std_path):
    """
    Clusters the new instances by the provided model. The new instances can be provided in different
    formats:
        - preprocessed data_frame format -> it clusters them and generates the result file
        - file containing a instance's overview -> prints the results
        - manually by cmd providing the plot -> prints the results.
    :param list tuples: list of (input_path_preprocessed_data_frame, output_path)
    :param list texts: list of paths to the txt file
    :param str model_path: path to the clustering model
    :param str doc_to_vec: path to the doc2Vec model
    :param str mean_path: path to the training vec mean vec
    :param str std_path: path to the training vec std vec
    """
    model = KMeans.load_model(model_path)
    doc_to_vec = DocToVec.load_model(doc_to_vec)
    standardization = Standardization.get_standardization(mean_path, std_path)
    classifier = Clusterer(doc_to_vec, model, standardization)
    if tuples is None and texts is None:
        classifier.manual()
    else:
        if tuples is not None:
            for input_path, output_path in tuples:
                my_data_frame = classifier.data_frame(MyDataFrame.get_data_frame(input_path))
                my_data_frame.save_data_frame(output_path)

        if texts is not None:
            for text in texts:
                with open(text, "r") as f:
                    classifier.classify_description(f.read())
                    f.close()


if __name__ == '__main__':
    parser = Config.get_parser(help_message, has_input=False, has_output=False)
    parser.add_argument("-m", "--model", nargs=1, dest="model", type=str, metavar="<model_path>",
                        required=True)
    parser.add_argument("-d", "--doc_to_vec", nargs=1, dest="doc_to_vec", type=str,
                        metavar="<model_path>",
                        required=True)
    parser.add_argument("-e", "--mean", nargs=1, dest="mean", type=str, metavar="<input_path>",
                        required=True)
    parser.add_argument("-std", "--std_deviation", nargs=1, dest="std", type=str,
                        metavar="<input_path>",
                        required=True)
    parser.add_argument("-f", "--frames", nargs=2, dest="tuples",
                        metavar=("<input>", "<output>"),
                        required=False, action=ActionTuple)
    parser.add_argument("-t", "--text", nargs="*", dest="texts",
                        metavar="<input>", type=str,
                        required=False, action="append")
    args = Config.get_args(parser)
    cluster_new_instances(args.tuples, args.texts, args.model[0], args.doc_to_vec[0], args.mean[0],
                          args.std[0])
