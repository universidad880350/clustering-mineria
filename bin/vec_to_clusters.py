from modules.clustering import InstancesVecDataFrame
from modules.settings import Config

help_message = '''
    Usage Example:
        python3.6 vec_to_clusters -i vec.csv -o out/
    
    Print Parser Help:
        python3.6 vec_to_clusters -h
            
'''


def clustering(input_path, output_folder):
    """
    Given a input of a instances dataset it generates the hierarchical clustering model and the additional info
    and it saves it in the given output folder. It will take into account the given configuration.
    :param input_path: It corresponds to the instances dataset required in the InstancesVecDataFrame format.
    :param output_folder: It corresponds to the folder path where the generated files with be stored.
    """
    instances = InstancesVecDataFrame.get_data_frame(input_path)
    print(len(instances))
    hierarchical_clustering = instances.create_hierarchical_clustering()
    hierarchical_clustering.clustering(output_folder)


if __name__ == '__main__':
    args = Config.handle_parser(help_message)
    clustering(args.input[0], args.output[0])
