from modules.clustering import UncleanedDataFrame
from modules.settings import Config

help_message = '''
    Usage Example:
        python3.6 preprocessed_to_docs -i preprocessed.csv -o clean.csv
        
    Print Parser Help:
        python3.6 vec_to_clusters -h
'''


def clean_doc(preprocessed_path: str, output_path: str):
    """
    Given a preprocessed data_frame applies a entity substitution and lemmatization of the tokens
    in each overview and returns the resulting data frame.
    :param str preprocessed_path: the location of the input preprocessed data frame
    :param str output_path:  where the resulting data frame will be stored
    :param Config config: settings of the program
    """
    uncleaned_data_frame = UncleanedDataFrame.get_data_frame(preprocessed_path)
    uncleaned_data_frame.set_dtypes({"overview": str})
    my_data_frame = uncleaned_data_frame.clean()
    my_data_frame.save_data_frame(output_path)


if __name__ == '__main__':
    args = Config.handle_parser(help_message)
    clean_doc(args.input[0], args.output[0])
